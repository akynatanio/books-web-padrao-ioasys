import React from 'react';
import { Switch } from 'react-router-dom';
import SignIn from '../pages/SignIn';
import Home from '../pages/Home';
import PrivateRoute from './routes/private';
import PublicRoute from './routes/public';


const Routes = () => (
  <Switch>
    <PublicRoute path="/" exact component={SignIn} />
    <PrivateRoute path="/home" exact component={Home} />
  </Switch>
);

export default Routes;
