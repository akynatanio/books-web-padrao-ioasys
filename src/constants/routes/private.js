import React from 'react';
import {
    Route,
    Redirect,
} from 'react-router-dom';

import {useAuth} from '../../hooks/useAuth';

const PrivateRoute = ({ component: Component, ...rest }) => {
    const { user } = useAuth();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                !!user ? (
                    <Component />
                ) : (
                    <Redirect
                        to={{
                            pathname: '/',
                            state: {
                                from: location,
                            },
                        }}
                    />
                )
            }
        />
    )
};

export default PrivateRoute;