import React from 'react';
import {
    Route,
    Redirect,
} from 'react-router-dom';

import {useAuth} from '../../hooks/useAuth';

const PublicRoute = ({ component: Component, ...rest }) => {
    const { user } = useAuth();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                !!user ? (
                    <Redirect
                        to={{
                            pathname: '/home',
                            state: {
                                from: location,
                            },
                        }}
                    />
                ) : (
                    <Component/>
                )
            }
        />
    )
};

export default PublicRoute;