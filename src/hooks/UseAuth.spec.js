import { renderHook, act } from '@testing-library/react-hooks';
import MockAdpter from 'axios-mock-adapter';

import api from '../services/api';
import { AuthProvider, useAuth } from '../hooks/useAuth';

const apiMock = new MockAdpter(api);

describe('Auth Hook', () => {
  it('should be able to sign in', async () => {
    apiMock.onPost('auth/sign-in').reply(200, {
      id: 'id-false',
      name: 'name-false',
      email: 'desafio@ioasys.com.br',
    }, {
        Authorization: 'token-false'
    });

    const setItemSpy = jest.spyOn(Storage.prototype, 'setItem');

    const { result, waitForNextUpdate } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    result.current.signIn({
      email: 'desafio@ioasys.com.br',
      password: '12341234',
    });

    await waitForNextUpdate();

    expect(setItemSpy).toHaveBeenCalledTimes(3);
    expect(result.current.user.email).toEqual('desafio@ioasys.com.br');
  });

  it('should restore saved data from storage when  auth inits', async () => {
    jest.spyOn(Storage.prototype, 'getItem').mockImplementation(key => {
      switch (key) {
        case '@DesafioIoasys:token':
          return 'token-false';
        case '@DesafioIoasys:user':
          return JSON.stringify({
            id: 'id-false',
            name: 'name-false',
            email: 'desafio@ioasys.com.br',
          });
        default:
          return null;
      }
    });

    const { result } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    expect(result.current.user.email).toEqual('desafio@ioasys.com.br');
  });

  it('should be able to signout', async () => {
    jest.spyOn(Storage.prototype, 'getItem').mockImplementation(key => {
      switch (key) {
        case '@DesafioIoasys:token':
          return 'token-false';
        case '@DesafioIoasys:user':
          return JSON.stringify({
            id: 'id-false',
            name: 'name-false',
            email: 'desafio@ioasys.com.br',
          });
        default:
          return null;
      }
    });

    const removeItemSpy = jest.spyOn(Storage.prototype, 'removeItem');

    const { result } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    act(() => {
      result.current.signOut();
    });

    expect(removeItemSpy).toHaveBeenCalledTimes(2);
    expect(result.current.user).toBeUndefined();
  });

});
