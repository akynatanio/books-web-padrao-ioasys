import styled, { css } from 'styled-components';

export const ContentLogo = styled.div`
  display: flex;
  color: #333333;

  ${props =>
    props.light &&
    css`
      color: #ffff;
    `}

  h1 {
    margin-left: 16px;
  }
`;
