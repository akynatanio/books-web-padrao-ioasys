import React from 'react';
import logoImg from '../../../assets/images/logo.svg';
import logoImgDark from '../../../assets/images/logo-dark.svg';

import { ContentLogo } from './Logo.styles';

const Logo = ({ light }) => (
  <ContentLogo light={!!light}>
    <img src={light ? logoImg : logoImgDark} alt="DesafioIoasys" />
    <h1>Books</h1>
  </ContentLogo>
);

export default Logo;
