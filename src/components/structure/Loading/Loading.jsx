import React from 'react';
import livroGif from '../../../assets/images/livro.gif';

import { Container } from './Loading.styles';

const Loading = () => (
  <Container>
    <img src={livroGif} alt="Livro Gif" />
  </Container>
);

export default Loading;
