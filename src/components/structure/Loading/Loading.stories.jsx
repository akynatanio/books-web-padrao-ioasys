import React from 'react';
import Loading from '.';

export default {
  title: 'Loading',
  component: Loading,
};

export const Default = (args) => <Loading {...args} />;
