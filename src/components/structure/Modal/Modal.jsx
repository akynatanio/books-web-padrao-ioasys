import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { FaRegTimesCircle } from 'react-icons/fa';
import { useSelector, useDispatch } from 'react-redux';
import { Creators as ModalBookActions } from "../../../store/ducks/modalBook";

import Loading from '../../structure/Loading';

import { Container, ContentModal, Book, ContentInformation } from './Modal.styles';

const ContainerModal = () => {
  const dispatch = useDispatch();

  const books = useSelector(state => {
    return state.books.data
  });

  const { open, idBook } = useSelector(state => {
    return state.modalBook
  });

  const [book, setBook] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    if (idBook !== '') {
      const newBook = books.find(book => book.id === idBook);
      setBook(newBook);
      setLoading(false);
    }
  }, [idBook]);

  return (
    <Container>
      <Modal
        isOpen={open}
        ariaHideApp={false}
        onRequestClose={() => dispatch(ModalBookActions.closeModal())}
        id="modal"
        style={{ content: { width: '70%', margin: 'auto' } }}
      >
        <ContentModal>
          <FaRegTimesCircle
            title="Fechar modal"
            size={30}
            color="red"
            onClick={() => dispatch(ModalBookActions.closeModal())}
          />
          {loading ? (
            <Loading />
          ) : (
            <>
              {book && (
                <Book>
                  <div className="title-book_mobile">
                    <h3>{book.title}</h3>
                    <span className="authors">{book.authors.join(', ')}</span>
                  </div>
                  <img src={book.imageUrl} alt="" />
                  <ContentInformation>
                    <div className="title-book_web">
                      <h3>{book.title}</h3>
                      <span className="authors">{book.authors.join(', ')}</span>
                    </div>
                    <div>
                      <h4>INFORMAÇÕES</h4>
                      <table>
                        <tbody>
                          <tr>
                            <td>Páginas</td>
                            <td className="vlr">{book.pageCount}</td>
                          </tr>
                          <tr>
                            <td>Editora</td>
                            <td className="vlr">{book.publisher}</td>
                          </tr>
                          <tr>
                            <td>Publicação</td>
                            <td className="vlr">{book.published}</td>
                          </tr>
                          <tr>
                            <td>Idioma</td>
                            <td className="vlr">{book.language}</td>
                          </tr>
                          <tr>
                            <td>Título Original</td>
                            <td className="vlr">{book.title}</td>
                          </tr>
                          <tr>
                            <td>ISBN-10</td>
                            <td className="vlr">{book.isbn10}</td>
                          </tr>
                          <tr>
                            <td>ISBN-13</td>
                            <td className="vlr">{book.isbn13}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div className="description">
                      <h4>RESENHA DA EDITORA</h4>
                      <span className="vlr">{book.description}</span>
                    </div>
                  </ContentInformation>
                </Book>
              )}
            </>
          )}
        </ContentModal>
      </Modal>
    </Container>
  );
};

export default ContainerModal;
