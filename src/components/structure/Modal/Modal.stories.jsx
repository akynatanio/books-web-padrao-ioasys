import React from 'react';
import Modal from '.';

export default {
  title: 'Modal',
  component: Modal,
};

export const Default = (args) => <Modal {...args} />;
