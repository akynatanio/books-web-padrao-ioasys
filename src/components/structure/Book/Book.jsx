import React from 'react';

import { Container } from './Book.styles';
import { Creators as ModalBookActions } from "../../../store/ducks/modalBook";
import { useDispatch } from 'react-redux';

const Book = ({
  id,
  title,
  authors,
  pageCount,
  imageUrl,
  published,
  publisher,
}) => {
  const dispatch = useDispatch();

  return (
    <Container onClick={() => dispatch(ModalBookActions.openModal({id}))}>
      <img src={imageUrl} alt="" />
      <div>
        <div>
          <h3>{title}</h3>
          {authors.map(author => (
            <span key={author} className="author">
              {author}
            </span>
          ))}
        </div>
        <div>
          <span>{pageCount} páginas</span>
          <span>Editora {publisher}</span>
          <span>Publicado em {published}</span>
        </div>
      </div>
    </Container>
  );
};

export default Book;
