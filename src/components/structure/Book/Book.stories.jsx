import React from 'react';
import Book from '.';

export default {
  title: 'Book',
  component: Book,
};

export const Default = (args) => <Book {...args} />;
