import React from 'react';
import Tooltip from '.';

export default {
  title: 'Tooltip',
  component: Tooltip,
};

export const Default = (args) => <Tooltip {...args} />;
