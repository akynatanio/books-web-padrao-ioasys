import React from 'react';
import Input from '.';

export default {
  title: 'Input',
  component: Input,
};

export const Default = (args) => <Input {...args} />;
