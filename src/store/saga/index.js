import { all } from "redux-saga/effects";
import * as BooksSaga from "./booksSaga";

function* Sagas() {
  yield all([
    BooksSaga.watcherSaga(),
  ]);
}

export default Sagas;
