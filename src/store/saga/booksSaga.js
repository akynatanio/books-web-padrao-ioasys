import { call, put, takeLatest } from 'redux-saga/effects';

import api from '../../services/api';

import { Types } from '../ducks/books';

function* loadBookRequest({payload}) {
    const { page_current, category_current } = payload;
    try {
        const response = yield call(api.get, `books?amount=12&page=${page_current}&category=${category_current}`);

        yield put({
            type: Types.LOAD_BOOK_SUCCESS,
            data: response.data
        });
    } catch(err) {
        yield put({
            type: Types.LOAD_BOOK_FAILURE,
            error: err,
        });
    }
}

export function* watcherSaga() {
    yield takeLatest(Types.LOAD_BOOK_REQUEST, loadBookRequest);
}