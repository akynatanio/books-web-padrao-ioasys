import { createStore, applyMiddleware } from 'redux';
import { history } from "../helpers/sharedHelpers";
import createSagaMiddleware from 'redux-saga';

import Reducers from "./ducks";
import Sagas from './saga';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(Reducers(history), applyMiddleware(sagaMiddleware));

sagaMiddleware.run(Sagas);

export default store;