import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
    openModal: ['payload'],
    closeModal: ['payload'],
});

const INITIAL_STATE = {
    open: true,
    idBook: '',
};

const openModal = (state, { payload } ) => {
    const { id } = payload;
    return ({
        ...state,
        open: true,
        idBook: id,
    });
}

const closeModal = (state) => {
    return ({
        ...state,
        open: false,
        idBook: '',
    });
}

export default createReducer(INITIAL_STATE, {
    [Types.OPEN_MODAL]: openModal,
    [Types.CLOSE_MODAL]: closeModal,
});
  