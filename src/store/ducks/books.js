import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
    loadBookRequest: ['payload'],
    loadBookSuccess: ['payload', 'is_loading', 'data', 'totalPages'],
    loadBookFailure: ['payload', 'is_loading', 'error'],
});

const INITIAL_STATE = {
    data: [],
    is_loading: true,
    category_current: 'biographies',
    page_current: 1,
    total_pages: 0,
    categories: [
        { value: 'biographies', label: 'biographies' },
        { value: 'collections', label: 'collections' },
        { value: 'behavior', label: 'behavior' },
        { value: 'tales', label: 'tales' },
        { value: 'literary-criticism', label: 'literary-criticism' },
        { value: 'scienceFiction', label: 'scienceFiction' },
        { value: 'folklore', label: 'folklore' },
        { value: 'genealogy', label: 'genealogy' },
        { value: 'humor', label: 'humor' },
        { value: 'children', label: 'children' },
        { value: 'games', label: 'games' },
        { value: 'newspapers', label: 'newspapers' },
        { value: 'brazilian-literature', label: 'brazilian-literature' },
        { value: 'foreign-literature', label: 'foreign-literature' },
        { value: 'rare-books', label: 'rare-books' },
        { value: 'manuscripts', label: 'manuscripts' },
        { value: 'poetry', label: 'poetry' },
        { value: 'anothe', label: 'anothe' }
    ]
};

const loadBookRequest = (state, { payload }) => {
    const {page_current, category_current} = payload;

    return ({
        ...state,
        page_current,
        category_current,
        is_loading: true
    });
}

const loadBookSuccess = (state, payload) => {
    const {data, totalPages, page} = payload.data;

    return ({
        ...state,
        is_loading: false,
        data,
        page_current: page,
        total_pages: totalPages
    });
}

const loadBookFailure = (state, error) => ({
    ...state,
    is_loading: false,
    ...error,
});

export default createReducer(INITIAL_STATE, {
    [Types.LOAD_BOOK_REQUEST]: loadBookRequest,
    [Types.LOAD_BOOK_SUCCESS]: loadBookSuccess,
    [Types.LOAD_BOOK_FAILURE]: loadBookFailure,
});
  