import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import books from "./books";
import modalBook from "./modalBook";

const appReducer = (history) => {
  const reducers = {
    router: connectRouter(history),
    books,
    modalBook,
  };

  return combineReducers(reducers);
};

const Reducers = (history) => appReducer(history);

export default Reducers;
