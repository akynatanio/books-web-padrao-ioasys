import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import {Provider} from 'react-redux';
import store from './store/store';

import GlobalStyle from './styles/Global.styles';

import Routes from './constants/endpoints';

import AppProvider from './hooks';

const App = () => (
  <Provider store={store}>
    <Router>
        <AppProvider>
          <Routes />
        </AppProvider>
        <GlobalStyle />
    </Router>
  </Provider>
);
export default App;
