import React, { useRef, useCallback, useState } from 'react';
import { Form } from '@unform/web';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';

import getValidationErrors from '../../utils/getValidationErrors';

import Input from '../../components/form/Input/Input.jsx';
import Button from '../../components/form/Button/Button.jsx';
import Logo from '../../components/structure/Logo';

import { Container, Content, AnimationContainer, ErroLogin } from './styles';

const SignIn = () => {
  const [erro, setErro] = useState(false);
  const formRef = useRef();
  const { signIn } = useAuth();

  const history = useHistory();

  const handleErro = useCallback(err => {
    setErro(err);
  }, []);

  const handleSubmit = useCallback(
    async (data) => {
      formRef.current.setErrors({});
      try {
        const schema = Yup.object().shape({
          email: Yup.string()
            .required('E-mail obrigatório')
            .email('Digite um e-mail válido'),
          password: Yup.string().required('Senha obrigatória'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        await signIn({
          mail: data.email,
          password: data.password
        });

        history.push('/home');
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);
          formRef.current.setErrors(errors);
          handleErro(false);
          return;
        }
        handleErro(true);
      }
    },
    [signIn, handleErro, history],
  );

  return (
    <Container>
      <Content>
        <AnimationContainer>
          <Logo light />
          <Form ref={formRef} onSubmit={handleSubmit}>
            <Input
              name="email"
              label="E-mail"
              placeholder="Digite seu e-mail"
            />
            <Input
              name="password"
              type="password"
              label="Senha"
              placeholder="Digite sua senha"
            >
              <Button type="submit">Entrar</Button>
            </Input>
          </Form>
          {erro && (
            <ErroLogin data-testid="erro-login">
              <span>Email e/ou senha incorretos.</span>
            </ErroLogin>
          )}
        </AnimationContainer>
      </Content>
    </Container>
  );
};

export default SignIn;
