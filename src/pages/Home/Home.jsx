import React, { useCallback, useEffect } from 'react';
import { FiArrowLeftCircle, FiArrowRightCircle } from 'react-icons/fi';
import { FaSignOutAlt } from 'react-icons/fa';
import Select from 'react-select';
import { useDispatch, useSelector } from 'react-redux';
import { Creators as BooksActions } from "../../store/ducks/books";

import Logo from '../../components/structure/Logo';
import Book from '../../components/structure/Book';
import Loading from '../../components/structure/Loading';
import ContainerModal from '../../components/structure/Modal';

import { useAuth } from '../../hooks/useAuth';

import {
  Container,
  Header,
  HeaderContent,
  Content,
  Pagination,
} from './styles';

const Home = () => {
  const books = useSelector(state => {
    return state.books.data
  });

  const categories = useSelector(state => {
    return state.books.categories
  });

  const loading = useSelector(state => state.books.is_loading);
  const category_current = useSelector(state => state.books.category_current);
  const page_current = useSelector(state => state.books.page_current);
  const total_pages = useSelector(state => Math.ceil(state.books.total_pages));
  const dispatch = useDispatch();

  const { signOut, user } = useAuth();
  const { name } = user;

  useEffect(() => {
    dispatch(BooksActions.loadBookRequest({page_current, category_current}));
  }, [dispatch, page_current, category_current]);

  const incrementPage = useCallback(() => {
    dispatch(BooksActions.loadBookRequest({page_current: page_current + 1, category_current}));
  }, [dispatch, page_current, category_current]);

  const decrementPage = useCallback(() => {
    dispatch(BooksActions.loadBookRequest({page_current: page_current -1, category_current}));
  }, [dispatch, page_current, category_current]);

  const handleCategory = useCallback(selectedCategory => {
    dispatch(BooksActions.loadBookRequest({page_current: 1, category_current: selectedCategory.value}));
  }, [dispatch]);

  return (
    <Container>
      <Header>
        <HeaderContent>
          <Logo />

          <div>
            <span>
              Bem vindo, <strong>{name}</strong>!
            </span>
            <button type="button" onClick={signOut} data-testid="btt-signout">
              <FaSignOutAlt size={20} />
            </button>
          </div>
        </HeaderContent>
      </Header>

      <Content>
        <div className="select-category">
          <label htmlFor="category">Categoria:</label>
          <Select
            id="category"
            defaultValue={{value: category_current, label: category_current}}
            options={categories}
            onChange={handleCategory}
            style={{ width: '200px!important' }}
            data-testid="select"
          />
        </div>
        {loading ? (
          <Loading />
        ) : (
          <div className="list-container">
            {books ? (
              books.map(book => {
                const {
                  id,
                  title,
                  authors,
                  pageCount,
                  imageUrl,
                  published,
                  publisher,
                } = book;
                return (
                  <Book
                    key={id}
                    id={id}
                    title={title}
                    authors={authors}
                    pageCount={pageCount}
                    imageUrl={imageUrl}
                    published={published}
                    publisher={publisher}
                  />
                );
              })
            ) : (
              <div>Nenhum resultado...</div>
            )}
          </div>
        )}
        <Pagination>
          <span>
            Pagina <span data-testid="page-current">{page_current}</span> de {total_pages}
          </span>
          <button
            type="button"
            disabled={page_current === 1}
            onClick={decrementPage}
            title="Anterior Pagina"
            data-testid="btt-decrement"
          >
            <FiArrowLeftCircle />
          </button>
          <button
            type="button"
            disabled={page_current === total_pages}
            onClick={incrementPage}
            title="Pagina Próxima"
            data-testid="btt-increment"
          >
            <FiArrowRightCircle />
          </button>
        </Pagination>
      </Content>
      <ContainerModal />
    </Container>
  );
};

export default Home;
