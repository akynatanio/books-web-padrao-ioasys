import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import Home from './Home';
import { Provider } from 'react-redux';
import store from '../../store/store';

const mockedSignOut = jest.fn();

jest.mock("react-select", () => ({ options, value, onChange }) => {
  function handleCategory(event) {
    const option = options.find(
      (option) => option.value === event.currentTarget.value
    );
    onChange(option);
  }

  return (
    <select data-testid="select" value={value} onChange={handleCategory}>
      {options.map(({ label, value }) => (
        <option key={value} value={value}>
          {label}
        </option>
      ))}
    </select>
  );
});

jest.mock('../../hooks/useAuth', () => {
  return {
    useAuth: () => ({
      signOut: mockedSignOut,
      user: {
        name: 'Aky'
      }
    }),
  };
});

describe('Home Page', () => {
  it('should be able to render page home', async () => {
    const { getByTestId } = render(<Provider store={store}><Home /></Provider>);

    const buttonPageAnterior = getByTestId('btt-decrement');
    const bttSignOut = getByTestId('btt-signout');

    await waitFor(() => {
      expect(buttonPageAnterior).toBeDisabled();
      expect(bttSignOut).toBeTruthy();
    });
  });

  it('should be able signout', async () => {
    const { getByTestId } = render(<Provider store={store}><Home /></Provider>);

    const bttSignOut = getByTestId('btt-signout');

    fireEvent.click(bttSignOut);

    await waitFor(() => {
      expect(mockedSignOut).toHaveBeenCalled();
    });
  });

  it('should be able increment and decrement page', async () => {
    const { getByTestId } = render(<Provider store={store}><Home /></Provider>);

    const bttIncrement = getByTestId('btt-increment');
    const bttDecrement = getByTestId('btt-decrement');
    const pageCurrent = getByTestId('page-current');

    fireEvent.click(bttIncrement);
    expect(pageCurrent.textContent).toEqual("2");
    fireEvent.click(bttDecrement);
    expect(pageCurrent.textContent).toEqual("1");
  });

  it('should be able handle category', async () => {
    const { getByTestId } = render(<Provider store={store}><Home /></Provider>);

    const alterCategory = getByTestId('select');

    fireEvent.change(alterCategory, {
      target: { value: 'genealogy' },
    });

    await waitFor(() => {
      expect(alterCategory.value).toEqual('genealogy');
    });
  });
});
